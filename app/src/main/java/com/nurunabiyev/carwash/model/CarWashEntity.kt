package com.nurunabiyev.carwash.model

data class CarWash(
    val id: Long,
    val location: Location?,
    val name: String,
    val description: String,
    val imageUrl: String,
    val contact: List<String>, // email; phone numbers
    var isFavorite: Boolean = false // todo move state to db
    // todo available timeframes
    // todo available services
)

data class Location(
    val latLong: Pair<Double, Double>,
    val street: String,
    val city: String,
)