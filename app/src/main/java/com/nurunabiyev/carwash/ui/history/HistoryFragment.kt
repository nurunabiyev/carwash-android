package com.nurunabiyev.carwash.ui.history

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.nurunabiyev.carwash.R
import com.nurunabiyev.carwash.databinding.FragmentHistoryBinding
import com.nurunabiyev.carwash.model.CarWash
import kotlinx.android.synthetic.main.fragment_history.*

class HistoryFragment : Fragment() {

    companion object {
        const val TAG = "historylogtag"
    }

    private lateinit var historyVM: HistoryViewModel
    private var _binding: FragmentHistoryBinding? = null
    private val binding get() = _binding!!
    private var carWashAdapter: CarWashHistoryAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        historyVM = ViewModelProvider(this)[HistoryViewModel::class.java]
        _binding = FragmentHistoryBinding.inflate(inflater, container, false)
        val root: View = binding.root
        historyVM.carWashes.observe(viewLifecycleOwner) { carWashAdapter?.addNewList(it) }
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        historyRV?.layoutManager = LinearLayoutManager(activity)
        carWashAdapter = CarWashHistoryAdapter()
        historyRV?.adapter = carWashAdapter
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    class CarWashHistoryAdapter :
        RecyclerView.Adapter<CarWashHistoryAdapter.CarWashViewHolder>() {
        private val mCarWashList: ArrayList<CarWash> = ArrayList()

        fun addNewList(list: List<CarWash>) {
            mCarWashList.clear()
            mCarWashList.addAll(list)
            notifyDataSetChanged()
        }

        fun refreshFavorites() {
            mCarWashList.sortBy { !it.isFavorite }
            notifyDataSetChanged()
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
            CarWashViewHolder(parent)

        override fun onBindViewHolder(holder: CarWashViewHolder, position: Int) {
            holder.bind(mCarWashList[position])
            //setAnimation(holder.itemView, position) //todo animation not done yet
        }

        override fun getItemCount() = mCarWashList.size

        inner class CarWashViewHolder(parent: ViewGroup) : RecyclerView.ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.list_item_carwash, parent, false)
        ) {
            private val nameTV: TextView? = itemView.findViewById(R.id.nameTV)
            private val deviceCV: CardView? = itemView.findViewById(R.id.device_cv)
            private val starIV: ImageView? = itemView.findViewById(R.id.star)

            fun bind(carWash: CarWash) {
                nameTV?.text = carWash.name
                deviceCV?.setOnClickListener {  }
                starIV?.setImageResource(
                    when (carWash.isFavorite) {
                        true -> R.drawable.ic_star_filled
                        false -> R.drawable.ic_star_border
                    }
                )
                starIV?.setOnClickListener {
                    when (carWash.isFavorite) {
                        true -> {
                            starIV.setImageResource(R.drawable.ic_star_border)
                            carWash.isFavorite = false
                        }
                        else -> {
                            starIV.setImageResource(R.drawable.ic_star_filled)
                            carWash.isFavorite = true
                        }
                    }
                    refreshFavorites()
                }
            }
        }

    }
}