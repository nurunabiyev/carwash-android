package com.nurunabiyev.carwash.ui.profile

import android.os.Bundle
import androidx.preference.PreferenceFragmentCompat
import com.nurunabiyev.carwash.R

class SettingsFragment : PreferenceFragmentCompat() {

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.root_preferences, rootKey)
        // todo send data after saving to server
    }
}