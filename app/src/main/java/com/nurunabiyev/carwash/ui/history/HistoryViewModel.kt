package com.nurunabiyev.carwash.ui.history

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.nurunabiyev.carwash.model.CarWash

class HistoryViewModel : ViewModel() {

    private val _carWashes = MutableLiveData<List<CarWash>>().apply {
        val list = arrayListOf(
            CarWash(
                id = 1,
                location = null,
                name = "Luxor",
                description = "Description luxor",
                imageUrl = "",
                contact = emptyList()
            ),
            CarWash(
                id = 1,
                location = null,
                name = "Baku Car Wash",
                description = "Description Baku Car Wash",
                imageUrl = "",
                contact = emptyList()
            ),
            CarWash(
                id = 1,
                location = null,
                name = "Washtec Baku",
                description = "Description Washtec Baku",
                imageUrl = "",
                contact = emptyList()
            )
        )
        value = list
    }
    val carWashes: LiveData<List<CarWash>> = _carWashes
}