This app will follow the following technical requirements:

* MVVM with Repository pattern
* Dependency injection with Hilt framework
* Unit testing with JUnit, UI testing with Espresso


We will follow iOS design for now:
![s1](iphone_screenshots/photo_2021-11-14 09.05.21.jpeg) 

![s2](iphone_screenshots/photo_2021-11-14 09.05.20.jpeg) 

![s3](iphone_screenshots/photo_2021-11-14 09.05.15.jpeg) 

![s4](iphone_screenshots/photo_2021-11-14 09.05.17.jpeg) 

![s5](iphone_screenshots/photo_2021-11-14 09.05.16.jpeg) 


![s6](iphone_screenshots/photo_2021-11-14 09.05.14.jpeg) 
